//ES5 -> ES6

//Primera implementacion

const http = require("http");

function server(request, response){
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("Hola mundo!!");
  response.end();
}

http.createServer(server).listen(8080);

//Segunda implementacion

const http = require("http");

http.createServer(function(request, response){
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("Hola mundo!!");
  response.end();
}).listen(8080);

//Tercera implementacion

const http = require("http");

const http = require("http");

http.createServer((request, response) => {
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("Hola mundo!!");
  response.end();
}).listen(8080);
